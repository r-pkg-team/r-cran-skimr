Source: r-cran-skimr
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Eric Brown <eb@ericebrown.com>
Section: gnu-r
Testsuite: autopkgtest-pkg-r
Priority: optional
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-cli,
               r-cran-dplyr (>= 0.8.0),
               r-cran-knitr,
               r-cran-magrittr,
               r-cran-pillar (>= 1.6.4),
               r-cran-purrr,
               r-cran-repr,
               r-cran-rlang,
               r-cran-stringr,
               r-cran-tibble,
               r-cran-tidyr (>= 1.0),
               r-cran-tidyselect (>= 1.0.0),
               r-cran-vctrs
Standards-Version: 4.6.2
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-cran-skimr
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-cran-skimr.git
Homepage: https://cran.r-project.org/package=skimr
Rules-Requires-Root: no

Package: r-cran-skimr
Architecture: all
Depends: ${R:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R compact and flexible summaries of data
 A simple to use summary function that can be used with pipes
 and displays nicely in the console. The default summary statistics may
 be modified by the user as can the default formatting.  Support for
 data frames and vectors is included, and users can implement their own
 skim methods for specific object types as described in a vignette.
 Default summaries include support for inline spark graphs.
 Instructions for managing these on specific operating systems are
 given in the "Using skimr" vignette and the README.
